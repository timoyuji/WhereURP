package com.whereta.filter;

import com.whereta.utils.ThreadLocalUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Vincent
 * @time 2015/8/12 16:27
 */
public class BaseFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        ThreadLocalUtil.setBasePath(basePath);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {

    }
}

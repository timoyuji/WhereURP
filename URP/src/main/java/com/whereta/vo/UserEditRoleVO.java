package com.whereta.vo;

import javax.validation.constraints.NotNull;

/**
 * Created by vincent on 15-9-21.
 */
public class UserEditRoleVO {
    @NotNull(message = "用户id不能为空")
    private Integer userId;
    @NotNull(message = "角色id不能为空")
    private String roleIds;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }
}
